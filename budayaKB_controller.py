"""
TEMPLATE TP4 DDP1 Semester Gasal 2019/2020
Template Author: 
Ika Alfina (ika.alfina@cs.ui.ac.id)
Evi Yulianti (evi.yulianti@cs.ui.ac.id)
Meganingrum Arista Jiwanggi (meganingrum@cs.ui.ac.id)

Project Author: Raihan Rizqi Muhtadiin (raihan.rizqi91@ui.ac.id)

Last modified: 9 December 2019
"""
# Impor budayaKB_model sebagai aplikasi kode Python di web dan flask sebagai web framework
from budayaKB_model import BudayaItem, BudayaCollection
from flask import Flask, request, render_template, redirect, flash

"""
Membuat objek app sebagai instance dari class Flask yang diimpor dari package flask.
app.secret_key perlu ada agar aplikasi flask bisa dijalankan.
"""
app = Flask(__name__)
app.secret_key ="inimerupakansecretkeydaritugaspemrograman4(tp4)hasilkaryasaya"

# Inisialisasi objek budayaData dan menyiapkan file default untuk digunakan selanjutnya
databasefilename = "default.csv"
budayaData = BudayaCollection()

# Me-render tampilan awal web
@app.route('/')
@app.route('/index')
def index():
	return render_template("index.html")

"""
Bagian ini adalah implementasi fitur "Impor Data Budaya", yaitu:
1) me-render tampilan saat menu Impor Data Budaya diklik	
2) melakukan pemrosesan terhadap file hasil impor setelah tombol "Impor Data" diklik
3) menampilkan notifikasi bahwa data telah berhasil diimpor
"""
@app.route('/imporBudaya', methods=['GET', 'POST'])
def imporData():
	if request.method == "GET":
		return render_template("imporBudaya.html")
	elif request.method == "POST":
		f = request.files['file']
		global databasefilename
		databasefilename = f.filename
		n_data = budayaData.importFromCSV(f.filename)
		return render_template("imporBudaya.html", result=n_data, fname=f.filename)

"""
Bagian ini adalah implementasi fitur "Tambah Data" yang ada pada dropdown content "Sunting Data Budaya", yaitu:
1) me-render tampilan saat menu Tambah Data diklik
2) melakukan pemrosesan terhadap isian form setelah tombol "Tambah Data" diklik
3) jika tambah data berhasil dilakukan, data tambahan tersebut akan langsung terekspor ke database yang sudah ada.
4) menampilkan notifikasi baik ketika proses tambah data berhasil dilakukan ataupun tidak.
"""
@app.route('/tambahBudaya', methods=['GET', 'POST'])
def tambahData():
	if request.method == 'GET':
		return render_template("tambahBudaya.html")
	elif request.method == 'POST':
		nama = request.form["nama"]
		tipe = request.form["tipe"]
		prov = request.form["prov"]
		url = request.form["url"]
		penyatu_data = budayaData.tambah(nama, tipe, prov, url)
		budayaData.exportToCSV(databasefilename)
		return render_template("tambahBudaya.html", nama=nama, tipe=tipe, prov=prov, url=url, penyatu_data=penyatu_data)

"""
Bagian ini adalah implementasi fitur "Update Data" yang ada pada dropdown content "Sunting Data Budaya", yaitu:
1) me-render tampilan saat menu Update Data diklik
2) melakukan pemrosesan terhadap isian form setelah tombol "Update Data" diklik
3) jika update data berhasil dilakukan, data hasil update tersebut akan menggantikan data relevan yang sudah ada.
4) menampilkan notifikasi baik ketika proses update data berhasil dilakukan ataupun tidak.
"""
@app.route('/updateBudaya', methods=['GET', 'POST'])
def updateData():
	if request.method == 'GET':
		return render_template("updateBudaya.html")
	elif request.method == 'POST':
		nama = request.form["nama"]
		tipe = request.form["tipe"]
		prov = request.form["prov"]
		url = request.form["url"]
		pengupdate_data = budayaData.ubah(nama, tipe, prov, url)
		budayaData.exportToCSV(databasefilename)
		return render_template("updateBudaya.html", nama=nama, tipe=tipe, prov=prov, url=url, pengupdate_data=pengupdate_data)

"""
Bagian ini adalah implementasi fitur "Hapus Data" yang ada pada dropdown content "Sunting Data Budaya", yaitu:
1) me-render tampilan saat menu Hapus Data diklik
2) melakukan pemrosesan terhadap isian form setelah tombol "Hapus Data" diklik
3) jika hapus data berhasil dilakukan, data yang ingin dihapus akan langsung menghilang dari database
4) menampilkan notifikasi baik ketika proses hapus data berhasil dilakukan ataupun tidak
"""
@app.route('/hapusBudayatapijanganhapusBudayadarihatikalian', methods=['GET', 'POST'])
def hapusData():
	if request.method == 'GET':
		return render_template("hapusBudaya.html")
	elif request.method == 'POST':
		nama = request.form["nama"]
		print(nama)
		penghapus_data = budayaData.hapus(nama)
		budayaData.exportToCSV(databasefilename)
		return render_template("hapusBudaya.html", nama=nama, penghapus_data=penghapus_data)

"""
Bagian ini adalah implementasi fitur "Cari Data Budaya", yaitu:
1) me-render tampilan saat menu Cari Data Budaya diklik
2) melakukan pemrosesan terhadap pilihan kategori dan keyword yang dicari
3) menampilkan notifikasi baik ketika keyword dengan kategori yang dicari terdapat dalam database maupun tidak
4) jika keyword dengan kategori yang dicari terdapat dalam database, tampilkan tabel berisi data yang relevan dengan keywordz
"""
@app.route('/cariBudaya', methods=['GET', 'POST'])
def cariData():
	queryNama = ""
	if request.method == 'GET':
		return render_template('cariBudaya.html')
	elif request.method == 'POST':
		kategori = request.form['kategori']
		queryNama = request.form['nama']
		if kategori == "nama budaya":
			resultCari = budayaData.cariByNama(queryNama)
		elif kategori == "tipe budaya":
			resultCari = budayaData.cariByTipe(queryNama)
		elif kategori == "asal provinsi budaya":
			resultCari = budayaData.cariByProv(queryNama)
	return render_template('cariBudaya.html', result=resultCari, nama=queryNama, kategori=kategori)

"""
Bagian ini adalah implementasi fitur "Statistik Data Budaya", yaitu:
1) me-render tampilan saat menu Statistik Data Budaya diklik
2) memproses statistik data budaya sesuai yang di-demand oleh user
"""
@app.route('/statsBudaya', methods=['GET', 'POST'])
def statsData():
	if request.method == 'GET':
		return render_template('statsBudaya.html')
	elif request.method == 'POST':
		kategoristat = request.form['kategoristat']
		if kategoristat == "Keseluruhan":
			resultStat = budayaData.stat()
		elif kategoristat == "tipeBudaya":
			resultStat = budayaData.statByTipe()
		elif kategoristat == "asalBudaya":
			resultStat = budayaData.statByProv()
	return render_template('statsBudaya.html', result=resultStat, kategoristat=kategoristat)

# Fitur tambahan 1
@app.route('/repoBudayaKBLiteCLIVersion', methods=['GET'])
def budayaKBLiteCLI():
	if request.method == 'GET':
		return render_template('budayaKBliteCLIVersion.html')

# Fitur tambahan 2
@app.route('/about', methods=['GET'])
def about():
	if request.method == 'GET':
		return render_template('aboutWeb.html')

# Untuk menjalankan program
if __name__ == "__main__":
	app.run(debug=True)